 #include "fastjet/ClusterSequence.hh"
 #include "fastjet/Selector.hh"
 #include <iostream> // needed for io
 #include <sstream>  // needed for io
 #include <cstdio>   // needed for io

using namespace std;

   class MyUserInfo : public fastjet::PseudoJet::UserInfoBase{
   public:
     MyUserInfo(const int & isCharged_in,const int & constitNum_in) :
       _isCharged(isCharged_in), _constitNum(constitNum_in){}
     int isCharged() const { return _isCharged;}
     int constitNum() const { return _constitNum;}

   protected:
     int _isCharged;
     int _constitNum;
   };

