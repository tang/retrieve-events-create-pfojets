#ifndef MYPACKAGE_MYPACKAGEALG_H
#define MYPACKAGE_MYPACKAGEALG_H 1

#include "AthAnalysisBaseComps/AthAnalysisAlgorithm.h"

#include "SUSYTools/ISUSYObjDef_xAODTool.h"

#include "GoodRunsLists/GoodRunsListSelectorTool.h"

#include "TrigDecisionTool/TrigDecisionTool.h"

#include "AnalysisCamEvent/CamEvent.h"

class MyPackageAlg: public ::AthAnalysisAlgorithm { 
 public: 
  MyPackageAlg( const std::string& name, ISvcLocator* pSvcLocator );
  virtual ~MyPackageAlg() override; 

  ///uncomment and implement methods as required

                                        //IS EXECUTED:
  virtual StatusCode  initialize();     //once, before any input is loaded
  virtual StatusCode  beginInputFile(); //start of each input file, only metadata loaded
  virtual StatusCode  execute();        //per event
  virtual StatusCode  finalize();       //once, after all events processed
  

 private: 

   void selectElectrons(xAOD::ElectronContainer* electrons, CamVector* camElectrons);

   void selectMuons(xAOD::MuonContainer* muons, CamVector* camMuons);

   void selectMuonsNotOverlap(xAOD::MuonContainer* muons, CamVector* camMuons);

   void selectPFOJets(CamVector* chargedJets, CamVector* allJets, CamVector* allTSJets, CamVector* camMuons);

   void selectJets(xAOD::JetContainer* jets, CamVector* camJets, int p, xAOD::Vertex pvtx, CamVector* camMuons);

   void selectLowPtJets(xAOD::JetContainer* jets, CamVector* camJets, int p, xAOD::Vertex pvtx, CamVector* camMuons);

   StatusCode setupSystematics();
   bool m_firstEvent = true;
   float m_totalEvents = 0.0;
   float m_totalWeightedEvents = 0.0;

   float m_isData = false;

////////declare member variables //////////

   ToolHandle<ST::ISUSYObjDef_xAODTool> m_SUSYTools;

   //ToolHandle<IGoodRunsListSelectorTool> m_GRLTool;

   std::vector<CP::SystematicSet> sysList;

}; 

#endif //> !MYPACKAGE_MYPACKAGEALG_H
