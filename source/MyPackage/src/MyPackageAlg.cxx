// MyPackage includes
#include "MyPackageAlg.h"
#include "MyUserInfo.h"
#include "xAODCore/ShallowCopy.h"
#include "xAODBase/IParticleHelpers.h"

#include "xAODEventInfo/EventInfo.h"
#include "xAODEventShape/EventShape.h"
#include "xAODTruth/TruthParticleContainer.h"
#include "xAODTruth/TruthVertex.h"
#include "xAODTruth/TruthVertexContainer.h"
#include "xAODTracking/TrackParticle.h"
#include "xAODTracking/TrackParticleContainer.h"
#include "xAODTracking/TrackParticlexAODHelpers.h"
#include "xAODTracking/VertexContainer.h"
#include "xAODCaloEvent/CaloCluster.h"
#include "xAODCaloEvent/CaloClusterContainer.h"

#include "xAODMuon/MuonContainer.h"
#include "xAODMuon/MuonSegmentContainer.h"
#include "xAODEgamma/PhotonContainer.h"
#include "xAODEgamma/ElectronContainer.h"
#include "xAODJet/JetContainer.h"
#include "xAODJet/JetAuxContainer.h"
#include "xAODMissingET/MissingETAuxContainer.h"

#include "xAODCutFlow/CutBookkeeperContainer.h"
#include "xAODCutFlow/CutBookkeeper.h"

#include "xAODPFlow/PFOContainer.h"
#include "fastjet/ClusterSequence.hh"
#include <iostream>
#include "cstdio"
using namespace std;

MyPackageAlg::MyPackageAlg( const std::string& name, ISvcLocator* pSvcLocator ) : AthAnalysisAlgorithm( name, pSvcLocator ){

  //declareProperty( "Property", m_nProperty = 0, "My Example Integer Property" ); //example property declaration

}


MyPackageAlg::~MyPackageAlg() {

}


StatusCode MyPackageAlg::initialize() {
  ATH_MSG_INFO ("Initializing " << name() << "...");
  //
  //This is called once, before the start of the event loop
  //Retrieves of tools you have configured in the joboptions go here
  //

   m_SUSYTools.setTypeAndName("ST::SUSYObjDef_xAOD/SUSYTools");
   CHECK( m_SUSYTools.retrieve() );


  return StatusCode::SUCCESS;
}

void MyPackageAlg::selectElectrons(xAOD::ElectronContainer* electrons, CamVector* camElectrons) {
   for(auto el : *electrons) {
      if (!el->auxdata<char>("baseline")) continue;
      CamObject tmp(*el);
      bool isSignal = el->auxdata<char>("signal");
      tmp.set("charge") = el->auxdata<float>("charge");
      tmp.set("passOR") = el->auxdata<char>("passOR");
      tmp.set("signal") = isSignal;
      camElectrons->push_back(tmp);
   }
}

void MyPackageAlg::selectMuons(xAOD::MuonContainer* muons, CamVector* camMuons) {
   for(auto mu : *muons) {
      if (!mu->auxdata<char>("baseline")) continue;
      CamObject tmp(*mu);
      bool isSignal = mu->auxdata<char>("signal");
      tmp.set("charge") = mu->auxdata<float>("charge");
      tmp.set("passOR") = mu->auxdata<char>("passOR");
      tmp.set("signal") = isSignal;
      camMuons->push_back(tmp);
   }
}

void MyPackageAlg::selectMuonsNotOverlap(xAOD::MuonContainer* muons, CamVector* camMuons) {
   for(auto mu : *muons) {
      if (!mu->auxdata<char>("baseline")) continue;
      if (!mu->auxdata<char>("signal")) continue;
      CamObject tmp(*mu);
      camMuons->push_back(tmp);
   }
} // select the muons which would be used in PFO selection 

void MyPackageAlg::selectPFOJets(CamVector* chargedJets, CamVector* allJets, CamVector* allTSJets, CamVector* camMuons) {
   const xAOD::JetContainer * tr_jets = 0;
   if (!m_isData){
     if ( !evtStore()->retrieve(tr_jets,"AntiKt4TruthJets").isSuccess() ) { //Change the string to the jet collection of interest
       Error("execute()", "Failed to retrieve jet  collection. Exiting." );
     }
   }

   camMuons->sort_Pt();
   std::vector<TLorentzVector> muonsvector;
   muonsvector = camMuons->getTLVVector();

   double vertexZ;
   //double vz;
   
   const xAOD::VertexContainer* pv = nullptr;
   if ( !evtStore()->retrieve(pv, "PrimaryVertices").isSuccess() ){ 
     Error("execute()", "Failed to retrieve Primary Vertex container. Exiting." );
   }
   const xAOD::Vertex_v1* vertex(0);
   //xAOD::VertexContainer::const_iterator vx = pv->begin();
   //for ( vx = pv->begin(); vx != pv->end(); ++vx ){
   //  if((*vx)->vertexType() == xAOD::VxType::PriVtx){
   //    vertexZ = (*vx)->z();
   //    vertex = (*vx);
   //    cout<<"vertexZ = "<<vertexZ<<endl;
   //    break;
   //  }//If we have a vertex of type primary vertex
   //}//iterate over the vertices and check their type
   for ( auto vx : *pv){
     if(vx->vertexType() == xAOD::VxType::PriVtx){
       vertex = vx;
       vertexZ = vertex->z();
       //cout<<"vertexZ = "<<vertexZ<<endl;
       break;
     }//If we have a vertex of type primary vertex
   }//iterate over the vertices and check their type
   //const xAOD::Vertex_v1* vertex1 = (*pv)[0]; //assuming first vertex is the correct one
   //double vertexZ1 = vertex1->z();
   //cout<<"vertexZ1 = "<<vertexZ1<<endl;
   
   const xAOD::TrackParticleContainer* IDT = nullptr;
   if ( !evtStore()->retrieve(IDT, "InDetTrackParticles").isSuccess() ){ 
     Error("execute()", "Failed to retrieve InDetTrackParticles container. Exiting." );
   }      
   auto tp = (*IDT)[0]; //vz should be the same for this event, use any track
   double vz = tp->vz();
   //cout<<"vz1 = "<<vz1<<endl;

////// retrieve the charged PFOs ///////

   const xAOD::PFOContainer* charged = 0;
   if( !evtStore()->retrieve(charged, "JetETMissChargedParticleFlowObjects").isSuccess() ){
   Error("execute()", "Failed to retrieve charged PFO container. Exiting." );
   
   }

////// select the charged PFOs which can be used to reconstrct jets //////

   std::vector<fastjet::PseudoJet> input_particles_charged_forall;
   xAOD::PFOContainer::const_iterator charge = charged->begin();
   for( charge = charged->begin();charge != charged->end(); ++charge ){
      //const xAOD::TrackParticle* ptrk = (*charge)->track(0);
      //if ( ptrk == 0 ) {
      //  ATH_MSG_WARNING("Skipping charged PFO with null track pointer.");
      //  continue;
      //}
      //double vz = ptrk->vz();
      //cout<<"vz = "<<vz<<endl;
      double cPFO_weight = (*charge)->auxdata<float>("DFCommonPFlow_envWeight");
      double z0    = (*charge)->auxdata<float>("DFCommonPFlow_z0");
      double theta = (*charge)->auxdata<float>("DFCommonPFlow_theta");
      double z0sintheta = std::fabs( z0 + vz - vertexZ)*sin( theta );
      //cout<<"z0sintheta = "<<z0sintheta<<endl;
      if(muonsvector.size()!=2) continue;
      if( (*charge)->p4EM().DeltaR(muonsvector[0])<0.2 || (*charge)->p4EM().DeltaR(muonsvector[1])<0.2 || z0sintheta>0.5) continue;
      input_particles_charged_forall.push_back(fastjet::PseudoJet((*charge)->p4EM().Px()*cPFO_weight , (*charge)->p4EM().Py()*cPFO_weight , (*charge)->p4EM().Pz()*cPFO_weight , (*charge)->p4EM().E()*cPFO_weight));
   }
   for(unsigned int i=0; i<input_particles_charged_forall.size(); i++){
      input_particles_charged_forall[i].set_user_info(new MyUserInfo(1,1));
   } // add extra information (whether charged,constituents number) to charged PFOs

////// reconstruct charged jets(input of track-seeded jets) ////// 

   double R = 0.4;
   fastjet::JetDefinition jet_def(fastjet::antikt_algorithm, R);
   fastjet::ClusterSequence clust_seq_charged(input_particles_charged_forall, jet_def);

   double ptmin = 5.0;
   std::vector<fastjet::PseudoJet> inclusive_jets_charged;
   inclusive_jets_charged = sorted_by_pt(clust_seq_charged.inclusive_jets(ptmin));

   for(unsigned int i=0; i<inclusive_jets_charged.size(); i++) {
      const fastjet::PseudoJet & full = inclusive_jets_charged[i];
      std::vector<fastjet::PseudoJet> jet_constituents = full.constituents();
      inclusive_jets_charged[i].set_user_info(new MyUserInfo(1,jet_constituents.size()));
   } // add extra information (whether charged,constituents number) to charged jets

////// retrieve and select the neutral PFOs which can be used to reconstrct jets //////

   const xAOD::PFOContainer* neutrals = 0;
   if( !evtStore()->retrieve(neutrals, "JetETMissNeutralParticleFlowObjects").isSuccess() ){
   Error("execute()", "Failed to retrieve charged PFO container. Exiting." );
   
   }

   std::vector<fastjet::PseudoJet> input_particles_neutral;
   //for(auto neutral : *neutrals){

   //  if(muonsvector.size()!=2) continue;
   //  if( neutral->p4EM().DeltaR(muonsvector[0])<0.2 || neutral->p4EM().DeltaR(muonsvector[1])<0.2 ) continue;
   //  xAOD::PFO *myPFO = new xAOD::PFO();
   //  myPFO->makePrivateStore(neutral);

   //  myPFO->setP4(myPFO->GetVertexCorrectedEMFourVec(*vertex));

   //  input_particles_neutral.push_back(fastjet::PseudoJet(myPFO->p4().Px(),myPFO->p4().Py(),myPFO->p4().Pz(),myPFO->p4().E()));
   //}

   xAOD::PFOContainer::const_iterator neutral = neutrals->begin();
   for( neutral = neutrals->begin();neutral != neutrals->end(); ++neutral ){
      if(muonsvector.size()!=2) continue;
      if( (*neutral)->p4EM().DeltaR(muonsvector[0])<0.2 || (*neutral)->p4EM().DeltaR(muonsvector[1])<0.2 ) continue;
      input_particles_neutral.push_back(fastjet::PseudoJet((*neutral)->p4EM().Px(),(*neutral)->p4EM().Py(),(*neutral)->p4EM().Pz(),(*neutral)->p4EM().E()));
   }

   for(unsigned int i=0; i<input_particles_neutral.size(); i++){
      input_particles_neutral[i].set_user_info(new MyUserInfo(0,1));
   } // add extra information (whether charged,constituents number) to neutral PFOs   

////// reconstruct allPFO jets with chargedPFOs and neutralPFOs //////

   std::vector<fastjet::PseudoJet> input_particles_all;
   input_particles_all.insert(input_particles_all.end(),input_particles_charged_forall.begin(),input_particles_charged_forall.end());
   input_particles_all.insert(input_particles_all.end(),input_particles_neutral.begin(),input_particles_neutral.end());

   fastjet::JetDefinition jet_def_all(fastjet::antikt_algorithm, R);
   fastjet::ClusterSequence clust_seq_all(input_particles_all, jet_def_all);

   std::vector<fastjet::PseudoJet> inclusive_jets_all;
   inclusive_jets_all = sorted_by_pt(clust_seq_all.inclusive_jets(ptmin));

////// reconstruct allPFO track-seeded jets with charged jets and neutralPFOs //////

   std::vector<fastjet::PseudoJet> input_particles_allTS;
   input_particles_allTS.insert(input_particles_allTS.end(),inclusive_jets_charged.begin(),inclusive_jets_charged.end());
   input_particles_allTS.insert(input_particles_allTS.end(),input_particles_neutral.begin(),input_particles_neutral.end());

   fastjet::JetDefinition jet_def_allTS(fastjet::antikt_algorithm, R);
   fastjet::ClusterSequence clust_seq_allTS(input_particles_allTS, jet_def_allTS);

   std::vector<fastjet::PseudoJet> inclusive_jets_allTS;
   inclusive_jets_allTS = sorted_by_pt(clust_seq_allTS.inclusive_jets(ptmin));
   
////// determine whether the allPFO jets are fake jets and add some extra information //////

   for(unsigned int i=0; i<inclusive_jets_all.size(); i++) {
      CamObject tmp_all;
      if(inclusive_jets_all[i].pt()<5000.) continue;
      const fastjet::PseudoJet & full = inclusive_jets_all[i];
      const std::vector<fastjet::PseudoJet> jet_constituents = full.constituents();
      int sum = 0;
      int constitnum = 0;
      double ChargedConstitPt = 0;
      //constitnum = jet_constituents.size(); 
      for(unsigned int j=0; j<jet_constituents.size(); j++) {
         sum = sum + jet_constituents[j].user_info<MyUserInfo>().isCharged();
         constitnum = constitnum + jet_constituents[j].user_info<MyUserInfo>().constitNum(); // calculate the Constituents Number
         ChargedConstitPt = ChargedConstitPt + (jet_constituents[j].user_info<MyUserInfo>().isCharged() * jet_constituents[j].pt()); // calculate the sum of charged constituents pT
      }
      tmp_all.SetPtEtaPhiE(inclusive_jets_all[i].pt(), inclusive_jets_all[i].eta(), inclusive_jets_all[i].phi_std(), inclusive_jets_all[i].E()); 

      int isPUalljet = 1;
      if (!m_isData){
        for ( auto ijet : *tr_jets ) {
          float dR2=pow(ijet->eta()-inclusive_jets_all[i].eta(),2)+pow(acos(cos(ijet->phi())*cos(inclusive_jets_all[i].phi_std())+sin(ijet->phi())*sin(inclusive_jets_all[i].phi_std())),2);
          if (dR2<0.6*0.6 && ijet->pt()>4000.) isPUalljet=0; 
        }
      } // determine whether the allPFO jets are fake jets
      tmp_all.set("isPUJet") = isPUalljet; // add some extra information
      tmp_all.set("ConstituentsNumber") = constitnum;
      tmp_all.set("ChargedConstitPt") = ChargedConstitPt;
      if (sum == 0) tmp_all.set("withChargedParticle") = 0;
      if (sum > 0) tmp_all.set("withChargedParticle") = 1;
      allJets->push_back(tmp_all); // push back the allPFO jets to the vector "allJets"
   }

////// determine whether the allPFO track-seeded jets are fake jets and add some extra information //////

   for(unsigned int i=0; i<inclusive_jets_allTS.size(); i++) {
      CamObject tmp_allTS;
      if(inclusive_jets_allTS[i].pt()<5000.) continue;
      const fastjet::PseudoJet & full = inclusive_jets_allTS[i];
      const std::vector<fastjet::PseudoJet> jet_constituents = full.constituents();
      int sum = 0;
      int constitnum = 0;
      double ChargedConstitPt = 0;
      for(unsigned int j=0; j<jet_constituents.size(); j++) {
         sum = sum + jet_constituents[j].user_info<MyUserInfo>().isCharged();
         constitnum = constitnum + jet_constituents[j].user_info<MyUserInfo>().constitNum();
         ChargedConstitPt = ChargedConstitPt + (jet_constituents[j].user_info<MyUserInfo>().isCharged() * jet_constituents[j].pt());
      }
      tmp_allTS.SetPtEtaPhiE(inclusive_jets_allTS[i].pt(), inclusive_jets_allTS[i].eta(), inclusive_jets_allTS[i].phi_std(), inclusive_jets_allTS[i].E());

      int isPUallTSjet = 1;
      if (!m_isData){
        for ( auto ijet : *tr_jets ) {
          float dR2=pow(ijet->eta()-inclusive_jets_allTS[i].eta(),2)+pow(acos(cos(ijet->phi())*cos(inclusive_jets_allTS[i].phi_std())+sin(ijet->phi())*sin(inclusive_jets_allTS[i].phi_std())),2);
          if (dR2<0.6*0.6 && ijet->pt()>4000.) isPUallTSjet=0;
        }
      }
      tmp_allTS.set("isPUJet") = isPUallTSjet;
      tmp_allTS.set("ConstituentsNumber") = constitnum;
      tmp_allTS.set("ChargedConstitPt") = ChargedConstitPt;
      if (sum == 0) tmp_allTS.set("withChargedParticle") = 0;
      if (sum > 0) tmp_allTS.set("withChargedParticle") = 1;
      allTSJets->push_back(tmp_allTS);
   }

}

void MyPackageAlg::selectJets(xAOD::JetContainer* jets, CamVector* camJets, int p, xAOD::Vertex pvtx, CamVector* camMuons) {
   for(auto jet : *jets) bool isSignal = m_SUSYTools->IsSignalJet(*jet,20000.,2.8);//,-2.0);
   const xAOD::JetContainer * tr_jets = 0;
   if (!m_isData){
     if ( !evtStore()->retrieve(tr_jets,"AntiKt4TruthJets").isSuccess() ) { //Change the string to the jet collection of interest
       Error("execute()", "Failed to retrieve jet  collection. Exiting." );
     }
   }

////// loop the traditional PFlow jets in this event //////

   for(auto jet : *jets) {
      //if (!jet->auxdata<char>("passOR")) continue;
      if (jet->pt()<10000.) continue; 
      CamObject tmp(*jet); //CamObject will be pushed back to the Camvector 
      tmp.set("signal") = jet->auxdata<char>("signal");//,-2.0);
      tmp.set("passOR") = jet->auxdata<char>("passOR");
      tmp.set("baseline") = jet->auxdata<char>("baseline");
      tmp.set("Jvt") = jet->auxdata<float>("Jvt");
      tmp.set("jvt") = jet->auxdata<float>("jvt");
      tmp.set("DetectorEta") = jet->auxdata<float>("DetectorEta");
      tmp.set("numConstituents") = jet->numConstituents();
      tmp.set("ConstitScaleMomentum_pt") = jet->auxdata<float>("JetConstitScaleMomentum_pt");
      tmp.set("ConstitScaleMomentum_eta") = jet->auxdata<float>("JetConstitScaleMomentum_eta");
      tmp.set("ConstitScaleMomentum_phi") = jet->auxdata<float>("JetConstitScaleMomentum_phi");
      tmp.set("JvtJvfcorr") = jet->auxdata<float>("JvtJvfcorr");
      tmp.set("JvtRpt") = jet->auxdata<float>("JvtRpt")*jet->auxdata<float>("JetPileupScaleMomentum_pt")/jet->pt();
      std::vector<float> SumTrkPt500vec;
      SumTrkPt500vec = jet->auxdata<std::vector<float>>("SumPtTrkPt500");
      float SumTrkPt500 = -1;
      if (SumTrkPt500vec.size()>0) SumTrkPt500 = SumTrkPt500vec.at(p);
      tmp.set("SumTrkPt500") = SumTrkPt500;
      std::vector<float> SumTrkPt1000vec;
      SumTrkPt1000vec = jet->auxdata<std::vector<float>>("SumPtTrkPt1000");
      float SumTrkPt1000 = -1;
      if (SumTrkPt1000vec.size()>0) SumTrkPt1000 = SumTrkPt1000vec.at(p);
      tmp.set("SumTrkPt1000") = SumTrkPt1000;

////// determine whether the PFlow jets are fake jets /////

      int isTruthJet=0;
      int isPileUpJet=1;
      int truthJetPt=0;
      float deltaR=100;
      if (!m_isData){
        for ( auto ijet : *tr_jets ) {
          float dR2=pow(ijet->eta()-jet->eta(),2)+pow(acos(cos(ijet->phi())*cos(jet->phi())+sin(ijet->phi())*sin(jet->phi())),2);
          if (dR2<0.3*0.3 && ijet->pt()>10000.) isTruthJet=1;
          if (dR2<0.3*0.3 && ijet->pt()>10000. && ijet->pt()>truthJetPt) truthJetPt=ijet->pt();
          if (dR2<0.6*0.6 && ijet->pt()>4000.) isPileUpJet=0;
          if (dR2<deltaR) deltaR=dR2;
        }
      }
      tmp.set("deltaR_truth_reco") = deltaR;
      tmp.set("isTruthJet") = isTruthJet;
      tmp.set("isPileUpJet") = isPileUpJet;
      tmp.set("truthJetPt") = truthJetPt;

      camJets->push_back(tmp); // push back the CamObject(tmp) 
  }
}


void MyPackageAlg::selectLowPtJets(xAOD::JetContainer* jets, CamVector* camJets, int p, xAOD::Vertex pvtx, CamVector* camMuons) {
   //for(auto jet : *jets) bool isSignal = m_SUSYTools->IsSignalJet(*jet,20000.,2.8);//,-2.0);
   const xAOD::JetContainer * tr_jets = 0;
   if (!m_isData){
     if ( !evtStore()->retrieve(tr_jets,"AntiKt4TruthJets").isSuccess() ) { //Change the string to the jet collection of interest
       Error("execute()", "Failed to retrieve jet  collection. Exiting." );
     }
   }

////// loop the traditional PFlow jets in this event //////

   for(auto jet : *jets) {
      //if (!jet->auxdata<char>("passOR")) continue;
      if (jet->pt()<10000.) continue; 
      CamObject tmp(*jet); //CamObject will be pushed back to the Camvector 
      tmp.set("signal") = jet->auxdata<char>("signal");//,-2.0);
      tmp.set("passOR") = jet->auxdata<char>("passOR");
      tmp.set("baseline") = jet->auxdata<char>("baseline");
      tmp.set("Jvt") = jet->auxdata<float>("Jvt");
      tmp.set("jvt") = jet->auxdata<float>("jvt");
      tmp.set("DetectorEta") = jet->auxdata<float>("DetectorEta");
      tmp.set("numConstituents") = jet->numConstituents();
      tmp.set("ConstitScaleMomentum_pt") = jet->auxdata<float>("JetConstitScaleMomentum_pt");
      tmp.set("ConstitScaleMomentum_eta") = jet->auxdata<float>("JetConstitScaleMomentum_eta");
      tmp.set("ConstitScaleMomentum_phi") = jet->auxdata<float>("JetConstitScaleMomentum_phi");
      tmp.set("JvtJvfcorr") = jet->auxdata<float>("JvtJvfcorr");
      tmp.set("JvtRpt") = jet->auxdata<float>("JvtRpt")*jet->auxdata<float>("JetPileupScaleMomentum_pt")/jet->pt();
      std::vector<float> SumTrkPt500vec;
      SumTrkPt500vec = jet->auxdata<std::vector<float>>("SumPtTrkPt500");
      float SumTrkPt500 = -1;
      if (SumTrkPt500vec.size()>0) SumTrkPt500 = SumTrkPt500vec.at(p);
      tmp.set("SumTrkPt500") = SumTrkPt500;
      std::vector<float> SumTrkPt1000vec;
      SumTrkPt1000vec = jet->auxdata<std::vector<float>>("SumPtTrkPt1000");
      float SumTrkPt1000 = -1;
      if (SumTrkPt1000vec.size()>0) SumTrkPt1000 = SumTrkPt1000vec.at(p);
      tmp.set("SumTrkPt1000") = SumTrkPt1000;

////// determine whether the PFlow jets are fake jets /////

      int isTruthJet=0;
      int isPileUpJet=1;
      int truthJetPt=0;
      float deltaR=100;
      if (!m_isData){
        for ( auto ijet : *tr_jets ) {
          float dR2=pow(ijet->eta()-jet->eta(),2)+pow(acos(cos(ijet->phi())*cos(jet->phi())+sin(ijet->phi())*sin(jet->phi())),2);
          if (dR2<0.3*0.3 && ijet->pt()>10000.) isTruthJet=1;
          if (dR2<0.3*0.3 && ijet->pt()>10000. && ijet->pt()>truthJetPt) truthJetPt=ijet->pt();
          if (dR2<0.6*0.6 && ijet->pt()>4000.) isPileUpJet=0;
          if (dR2<deltaR) deltaR=dR2;
        }
      }
      tmp.set("deltaR_truth_reco") = deltaR;
      tmp.set("isTruthJet") = isTruthJet;
      tmp.set("isPileUpJet") = isPileUpJet;
      tmp.set("truthJetPt") = truthJetPt;

      camJets->push_back(tmp); // push back the CamObject(tmp) 
  }
}

StatusCode MyPackageAlg::finalize() {
  ATH_MSG_INFO ("Finalizing " << name() << "...");
  //
  //Things that happen once at the end of the event loop go here
  //


  return StatusCode::SUCCESS;
}

StatusCode MyPackageAlg::execute() {  
  ATH_MSG_DEBUG ("Executing " << name() << "...");

   m_SUSYTools->resetSystematics();
   CHECK( m_SUSYTools->ApplyPRWTool() );

    CamEvent evt;

    if (m_totalEvents!=0){
      evt.set("totalEvents") = m_totalEvents;
      m_totalEvents=0.0;
    }else{
      evt.set("totalEvents") = 0.0;
    }
    if (m_totalWeightedEvents!=0){
      evt.set("totalWeightedEvents") = m_totalWeightedEvents;
      m_totalWeightedEvents=0.0;
    }else{
      evt.set("totalWeightedEvents") = 0.0;
    }

   // Get the vertex.
   const xAOD::VertexContainer* pvtxs = 0;
   int pvIndex=0;
   int PV2trk=0;
   int PV4trk=0;
   int hasPV=0;
   float pvIz=0.;
   ATH_CHECK(evtStore()->retrieve(pvtxs, "PrimaryVertices"));
   if ( pvtxs == 0 || pvtxs->size()==0 ) {
     ATH_MSG_WARNING(" This event has no primary vertices " );
     return StatusCode::SUCCESS;
   }else{
     int p=0;
     for( const auto& vx : *pvtxs ) {
       if(vx->vertexType() == xAOD::VxType::PriVtx) pvIndex=p;
       if(vx->vertexType() == xAOD::VxType::PriVtx) hasPV=1;
       if(vx->vertexType() == xAOD::VxType::PriVtx) pvIz=vx->z();
       p++;
       if (vx->nTrackParticles()>=2) PV2trk++;
       if (vx->nTrackParticles()>=4) PV4trk++;
     }
   }
   const xAOD::Vertex& vtx = *pvtxs->at(pvIndex);

   xAOD::JetContainer* jets(0);
   xAOD::ShallowAuxContainer* jets_aux(0);
   CHECK( m_SUSYTools->GetJets(jets,jets_aux,true) );
   
   xAOD::JetContainer* LowPtJets = 0;
   if ( !evtStore()->retrieve(LowPtJets,"AntiKt4EMPFlowLowPtJets").isSuccess() ) { //Change the string to the jet collection of interest
       Error("execute()", "Failed to retrieve Low pT jet  collection. Exiting." );
   }
   //ANA_CHECK (evtStore()->retrieve( LowPtJets, "AntiKt4EMTopoLowPtJets"));

    xAOD::MissingETContainer* met = new xAOD::MissingETContainer;
    xAOD::MissingETAuxContainer* met_aux = new xAOD::MissingETAuxContainer;
    met->setStore(met_aux);

    const xAOD::EventInfo* ei = 0;
    if( evtStore()->retrieve( ei, "EventInfo" ).isFailure() ) {
      ATH_MSG_WARNING( "No EventInfo object could be retrieved" );
    }
    if (!ei) {
      ATH_MSG_ERROR( "Cannot retrieve the EventInfo" );
    }
    evt.set("RunNumber") = ei->runNumber();
    evt.set("lbn") = ei->lumiBlock();
    evt.set("bcid") = ei->bcid();
    if (!m_isData) evt.set("EventWeight") = ei->mcEventWeights().at(0);
    if (!m_isData) evt.set("ChannelNumber") = ei->mcChannelNumber();
    if (!m_isData) evt.set("PileupWeight") = m_SUSYTools->GetPileupWeight();
    if (m_isData) evt.set("EventWeight") = 1.0;
    if (m_isData) evt.set("ChannelNumber") = ei->runNumber();
    if (m_isData) evt.set("PileupWeight") = 1.0;
    evt.set("RunNumber") = ei->runNumber();
    evt.set("averageInteractionsPerCrossing") = ei->averageInteractionsPerCrossing();
    evt.set("actualInteractionsPerCrossing") = ei->actualInteractionsPerCrossing();

    evt.set("hasPV") = hasPV;

if (hasPV==1){

    evt.set("NPV_2trk") = PV2trk;
    evt.set("NPV_4trk") = PV4trk;
    xAOD::ElectronContainer* electrons(0);
    xAOD::ShallowAuxContainer* electrons_aux(0);
    CHECK( m_SUSYTools->GetElectrons(electrons,electrons_aux,true) );

    xAOD::MuonContainer* muons(0);
    xAOD::ShallowAuxContainer* muons_aux(0);
    CHECK( m_SUSYTools->GetMuons(muons,muons_aux,true) );
    
 
    int passBadMuon=1;
    for( const auto& mu : *muons ) {
      if(mu->auxdata<char>("bad") && mu->auxdata<char>("baseline")) passBadMuon=0;
    }

    CHECK( m_SUSYTools->GetMET(*met,jets,electrons,muons,0,0,true,true) );

    CHECK( m_SUSYTools->OverlapRemoval(electrons,muons,jets) ); //decorate objets with 'passOR' char say if removed 

    int passBadJet=1;
    evt.set("passBadJet") = ei->auxdata<char>("DFCommonJets_eventClean_LooseBad");
    evt.set("isBadBatman") = ei->auxdata<char>("DFCommonJets_isBadBatman");

    evt.set("MET_RefFinal_TST_et") = (*met)["Final"]->met();
    evt.set("MET_RefFinal_TST_etx") = (*met)["Final"]->mpx();
    evt.set("MET_RefFinal_TST_ety") = (*met)["Final"]->mpy(); 
    evt.set("MET_RefFinal_TST_SumET") = (*met)["Final"]->sumet();

    evt.set("MET_RefJet_TST_et") = (*met)["RefJet"]->met();
    evt.set("MET_RefJet_TST_etx") = (*met)["RefJet"]->mpx();
    evt.set("MET_RefJet_TST_ety") = (*met)["RefJet"]->mpy(); 
    evt.set("MET_RefJet_TST_SumET") = (*met)["RefJet"]->sumet();

    evt.set("vtx_nTracks") = vtx.nTrackParticles();
    evt.set("vtx_z") = vtx.z();
    evt.set("pvIz") = pvIz;

    evt.set("HLT_e24_lhtight_nod0_ivarloose") = m_SUSYTools->IsTrigPassed("HLT_e24_lhtight_nod0_ivarloose");
    evt.set("HLT_e26_lhtight_nod0_ivarloose") = m_SUSYTools->IsTrigPassed("HLT_e26_lhtight_nod0_ivarloose");
    evt.set("HLT_e26_lhtight_nod0") = m_SUSYTools->IsTrigPassed("HLT_e26_lhtight_nod0");
    evt.set("HLT_e60_medium") = m_SUSYTools->IsTrigPassed("HLT_e60_medium");
    evt.set("HLT_e60_lhmedium_nod0") = m_SUSYTools->IsTrigPassed("HLT_e60_lhmedium_nod0");
    evt.set("HLT_e140_lhloose_nod0") = m_SUSYTools->IsTrigPassed("HLT_e140_lhloose_nod0");
    evt.set("HLT_e300_etcut") = m_SUSYTools->IsTrigPassed("HLT_e300_etcut");

    evt.set("HLT_mu24_iloose") = m_SUSYTools->IsTrigPassed("HLT_mu24_iloose");
    evt.set("HLT_mu24_ivarloose") = m_SUSYTools->IsTrigPassed("HLT_mu24_ivarloose");
    evt.set("HLT_mu24_imedium") = m_SUSYTools->IsTrigPassed("HLT_mu24_imedium");
    evt.set("HLT_mu26_ivarmedium") = m_SUSYTools->IsTrigPassed("HLT_mu26_ivarmedium");
    evt.set("HLT_mu40") = m_SUSYTools->IsTrigPassed("HLT_mu40");
    evt.set("HLT_mu50") = m_SUSYTools->IsTrigPassed("HLT_mu50");
    evt.set("HLT_mu60_0eta105_msonly") = m_SUSYTools->IsTrigPassed("HLT_mu60_0eta105_msonly");

    CamVector* el = evt.createVector("el");
    CamVector* mu = evt.createVector("mu");
    CamVector* jet = evt.createVector("jet");
    CamVector* lowpt_jet = evt.createVector("lowpt_jet");
    CamVector* mu_jet = evt.createVector("mu_jet");
    CamVector* chargedPFOjet = evt.createVector("chargedPFOjet");
    CamVector* allPFOjet = evt.createVector("allPFOjet");
    CamVector* allPFOTSjet = evt.createVector("allPFOTSjet");
    
    selectElectrons(electrons,el);
    selectMuons(muons,mu);
    selectMuonsNotOverlap(muons,mu_jet);
    selectPFOJets(chargedPFOjet,allPFOjet,allPFOTSjet,mu_jet);
    selectJets(jets,jet,pvIndex,vtx,mu_jet);
    selectLowPtJets(LowPtJets,lowpt_jet,pvIndex,vtx,mu_jet);

    el->applyService("CamWriter/myWriter","el_");
    mu->applyService("CamWriter/myWriter","mu_");
    jet->applyService("CamWriter/myWriter","jet_");
    lowpt_jet->applyService("CamWriter/myWriter","lowpt_jet_");
    allPFOjet->applyService("CamWriter/myWriter","allPFOjet_");
    allPFOTSjet->applyService("CamWriter/myWriter","allPFOTSjet_");

//// retrieve the truth jets and save it//////

    if (m_isData==false){
      const xAOD::JetContainer * tr_jets = 0;
      if ( !evtStore()->retrieve(tr_jets,"AntiKt4TruthJets").isSuccess() ) { //Change the string to the jet collection of interest
        Error("execute()", "Failed to retrieve jet  collection. Exiting." );
        return StatusCode::SUCCESS;
      }
      CamVector* trjets = evt.createVector("trjets");
      for ( auto ijet : *tr_jets ) {
        CamObject tmp(*ijet);
        if (ijet->pt()>4000.) trjets->push_back(tmp); //creates a new camobject from the TLV
      }
      trjets->applyService("CamWriter/myWriter","trjet_");

    }

}//close PV cut

  delete met;
  delete met_aux;

  evt.applyService("CamWriter/myWriter");


  return StatusCode::SUCCESS;
}

StatusCode MyPackageAlg::beginInputFile() { 
  //
  //This method is called at the start of each input file, even if
  //the input file contains no events. Accumulate metadata information here
  //
  std::cout<< "m_totalEvents section " <<std::endl;

  if( inputMetaStore()->contains<xAOD::CutBookkeeperContainer>("CutBookkeepers") ) { //only found in MC15
    const xAOD::CutBookkeeperContainer* bks = 0;
    CHECK( inputMetaStore()->retrieve(bks, "CutBookkeepers") );
      const xAOD::CutBookkeeper* all = 0; int maxCycle=-1;
      for(auto cbk : *bks) { if(cbk->inputStream()=="StreamAOD" && cbk->name()=="AllExecutedEvents" && cbk->cycle()>maxCycle) { maxCycle=cbk->cycle(); all = cbk; } }
    std::cout<< "m_totalEvents: "<< all->sumOfEventWeights() <<std::endl; 
    m_totalWeightedEvents += all->sumOfEventWeights();
    m_totalEvents += all->nAcceptedEvents();
  }

  std::cout<<"Autoconfigure isData and isAtlfast"<<std::endl;
  std::string projectname = "";
  ATH_CHECK( retrieveMetadata("/TagInfo", "project_name", projectname) );
  if ( projectname == "IS_SIMULATION" ) m_isData = false;
  else if (projectname.compare(0, 4, "data") == 0 ) m_isData = true;
  else {
    std::cout<<"Failed to autoconfigure -- project_name matches neither IS_SIMULATION nor data!"<<std::endl;
    return StatusCode::FAILURE;
  }

  return StatusCode::SUCCESS;
}


