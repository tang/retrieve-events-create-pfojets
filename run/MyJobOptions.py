#---- Minimal joboptions -------

theApp.EvtMax=-1	                                     #says how many events to run over. Set to -1 for all events

#2017 MC
#jps.AthenaCommonFlags.FilesInput = ["/afs/cern.ch/work/c/cyoung/mc16_13TeV.361107.PowhegPythia8EvtGen_AZNLOCTEQ6L1_Zmumu.deriv.DAOD_JETM3.e3601_s3126_r10201_p3600/DAOD_JETM3.14787864._000817.pool.root.1"]

jps.AthenaCommonFlags.FilesInput = ["/eos/user/t/tang/mc16_13TeV/DAOD_JETM3.20277384._000240.pool.root.1"]
#xAOD:
import AthenaRootComps.ReadAthenaxAODHybrid               #FAST xAOD reading!

algseq = CfgMgr.AthSequencer("AthAlgSeq")

######
## If you are running on data then use the following to apply the GRL
######
#ToolSvc += CfgMgr.GoodRunsListSelectionTool("GRLTool",GoodRunsListVec=["data15_13TeV.periodAllYear_DetStatus-v89-pro21-02_Unknown_PHYS_StandardGRL_All_Good_25ns.xml","data16_13TeV.periodAllYear_DetStatus-v89-pro21-01_DQDefects-00-02-04_PHYS_StandardGRL_All_Good_25ns.xml","data17_13TeV.periodAllYear_DetStatus-v99-pro22-01_Unknown_PHYS_StandardGRL_All_Good_25ns_Triggerno17e33prim.xml","data18_13TeV.periodAllYear_DetStatus-v102-pro22-03_Unknown_PHYS_StandardGRL_All_Good_25ns_Triggerno17e33prim.xml"])
#ToolSvc += CfgMgr.GoodRunsListSelectionTool("GRLTool",GoodRunsListVec=["data17_13TeV.periodAllYear_DetStatus-v99-pro22-01_Unknown_PHYS_StandardGRL_All_Good_25ns_Triggerno17e33prim.xml"])
#filterSeq = CfgMgr.AthSequencer("AthFilterSeq")
#filterSeq += CfgMgr.GRLSelectorAlg(Tool=ToolSvc.GRLTool)

algseq += CfgMgr.MyPackageAlg()

svcMgr += CfgMgr.CamWriter("myWriter",TreeName="/MYSTREAM/MyTree")

svcMgr += CfgMgr.THistSvc()
svcMgr.THistSvc.Output += ["MYSTREAM DATAFILE='myfile.root' OPT='RECREATE'"]

ToolSvc += CfgMgr.ST__SUSYObjDef_xAOD("SUSYTools")
config_file = "SUSYTools/SUSYTools_Default.conf" #look in the data directory of SUSYTools for other config files
ToolSvc.SUSYTools.ConfigFile = config_file

ToolSvc.SUSYTools.AutoconfigurePRWTool = True
#2017
ToolSvc.SUSYTools.PRWLumiCalcFiles = ["physics_25ns_Triggerno17e33prim.lumicalc.OflLumi-13TeV-010.root"]
ToolSvc.SUSYTools.PRWActualMu2017File = "physics_25ns_Triggerno17e33prim.actualMu.OflLumi-13TeV-010.root"

# if we want to do it by hand
#ToolSvc.SUSYTools.PRWConfigFiles = ["PUweights.root"]

ToolSvc.SUSYTools.DataSource = 1  #configure to run on data 0, or MC 1


